package be.ipl.wazabi.test;

import java.util.Collection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.IUserUcc;
import be.ipl.wazabi.ucc.exception.UccException;

public class ScenarioPatie {

  private static IGameUcc gameUcc;
  private static IGameListUcc gameListUcc;
  private static IUserUcc userUcc;

  private static PlayerEntity mBleu;
  private static PlayerEntity mRouge;
  private static GameEntity game;

  public static void main(String[] args) throws UccException {

    try {
      Context jndi = new InitialContext();
      gameUcc = (IGameUcc) jndi.lookup("ejb:AAE_EAR/AAE_EJB/GameUcc!be.ipl.wazabi.ucc.IGameUcc");
      gameListUcc = (IGameListUcc) jndi
          .lookup("ejb:AAE_EAR/AAE_EJB/GameListUcc!be.ipl.wazabi.ucc.IGameListUcc");
      userUcc = (IUserUcc) jndi.lookup("ejb:AAE_EAR/AAE_EJB/UserUcc!be.ipl.wazabi.ucc.IUserUcc");
    } catch (NamingException e) {
      e.printStackTrace();
    }

    mBleu = userUcc.register("MageBleu", "123456");
    System.out.println("Le joueur MageBleu s'est inscrit");
    mRouge = userUcc.register("MageRouge", "123456");
    System.out.println("Le joueur MageRouge s'est inscrit");

    game = gameListUcc.createGame("Magic Duel");
    game = gameListUcc.joinGame(game.getId(), mBleu.getId());
    System.out.println("Le MageBleu cr�e une partie --> " + game.getId() + " : " + game.getName());

    game = gameListUcc.joinGame(game.getId(), mRouge.getId());
    System.out
        .println("Le MageBleu rejoint une partie --> " + game.getId() + " : " + game.getName());

    GamePlayerEntity gamePlayerMBleu =
        gameListUcc.getCurrentGame(mBleu.getId()).getPlayerInstance(mBleu.getId());
    GamePlayerEntity gamePlayerMRouge =
        gameListUcc.getCurrentGame(mRouge.getId()).getPlayerInstance(mRouge.getId());
    GamePlayerEntity currentplayer = game.getCurrentPlayer();
    int[] magicpointPlayers;

    while (game.getState() != GameEntity.State.ENDED) {
      game = gameListUcc.getGameById(game.getId());
      currentplayer = game.getCurrentPlayer();
      getStatPlayer(gamePlayerMBleu, gamePlayerMRouge);

      magicpointPlayers = resultDices(game, currentplayer);

      game = gameListUcc.getGameById(game.getId());

      if (game.getState() != GameEntity.State.ENDED) {
        currentplayer = game.getCurrentPlayer();
        getStatPlayer(gamePlayerMBleu, gamePlayerMRouge);

        playCard(currentplayer, magicpointPlayers);

        game = gameListUcc.getGameById(game.getId());
      }
    }

    System.out.println("\n\n" + game.getWinner().getUsername() + " gagne la partie .");
  }

  public static void playCard(GamePlayerEntity currentplayer, int[] magicpoint)
      throws UccException {
    if (currentplayer.getPlayer().getId() == mBleu.getId()) {
      for (CardEntity card : currentplayer.getPlayableCards()) {
        if (magicpoint[0] >= card.getEffect().getUseCost()) {
          if (card.getEffect().getCode() == 4 || card.getEffect().getCode() == 6
              || card.getEffect().getCode() == 9) {
            gameUcc.playCard(mBleu.getId(), card.getId(), new String[] {"" + mRouge.getId()});
          } else if (card.getEffect().getCode() == 5) {
            gameUcc.playCard(mBleu.getId(), -1, "test");
          } else if (card.getEffect().getCode() == 2) {
            gameUcc.playCard(mBleu.getId(), -1, "test");
          } else {
            gameUcc.playCard(mBleu.getId(), card.getId(), "test");
          }
          System.out.println("MageBleu : utilise la carte " + card.getEffect().getName());
          System.out.println("Effet de la carte : " + card.getEffect().getDescription());
          return;
        }
        gameUcc.playCard(mBleu.getId(), -1, "test");
        System.out.println("MageBleu ne joue pas de carte");
        return;
      }
    } else {
      for (CardEntity card : currentplayer.getPlayableCards()) {
        if (magicpoint[1] >= card.getEffect().getUseCost()) {
          if (card.getEffect().getCode() == 4 || card.getEffect().getCode() == 6
              || card.getEffect().getCode() == 9) {
            gameUcc.playCard(mRouge.getId(), card.getId(), new String[] {"" + mBleu.getId()});
          } else if (card.getEffect().getCode() == 5) {
            gameUcc.playCard(mRouge.getId(), -1, "test");
          } else if (card.getEffect().getCode() == 2) {
            gameUcc.playCard(mRouge.getId(), -1, "test");
          } else {
            gameUcc.playCard(mRouge.getId(), card.getId(), "test");
          }
          System.out.println("MageBleu : utilise la carte " + card.getEffect().getName());
          System.out.println("Effet de la carte : " + card.getEffect().getDescription());
          return;
        }
        gameUcc.playCard(mRouge.getId(), -1, "test");
        System.out.println("MageRouge ne joue pas de carte " + card.getEffect().getName());
        return;
      }
    }
  }

  public static void givedice(GamePlayerEntity currentplayer) throws UccException {

    if (currentplayer.getState() == GamePlayerEntity.State.THROWING_DICES) {
      if (currentplayer.getPlayer().getId() == mBleu.getId()) {
        gameUcc.giveDices(mBleu.getId(), new int[] {mRouge.getId()});
      } else {
        gameUcc.giveDices(mRouge.getId(), new int[] {mBleu.getId()});
      }
    }
  }

  public static void getStatPlayer(GamePlayerEntity gamePlayerMBleu,
      GamePlayerEntity gamePlayerMRouge) {
    gamePlayerMBleu = gameListUcc.getCurrentGame(mBleu.getId()).getPlayerInstance(mBleu.getId());
    gamePlayerMRouge = gameListUcc.getCurrentGame(mRouge.getId()).getPlayerInstance(mRouge.getId());


    System.out
        .println("\n\nMageBleu : \nNombre de d� = " + gamePlayerMBleu.getPlayableDices().size()
            + "   \nNombre de carte = " + gamePlayerMBleu.getPlayableCards().size() + "\n");

    System.out
        .println("\nMageRouge : \nNombre de d� = " + gamePlayerMRouge.getPlayableDices().size()
            + "   \nNombre de carte = " + gamePlayerMRouge.getPlayableCards().size() + "\n");

  }

  public static int[] resultDices(GameEntity game, GamePlayerEntity currentplayer)
      throws UccException {

    game = gameListUcc.getCurrentGame(game.getId());


    Collection<DiceEntity> dicesPlayableCurrentPlayer =
        gameUcc.rollDices(currentplayer.getPlayer().getId());
    System.out.println(currentplayer.getPlayer().getUsername() + " lance les runes ");

    int scoreDiceMBleu = 0;
    int scoreDiceMRouge = 0;

    for (DiceEntity dices : dicesPlayableCurrentPlayer) {
      if (dices.getValue() == DiceEntity.WAZABI_FACE) {
        System.out.print(" [ WAZABI ] ");
        if (currentplayer.getPlayer().getId() == mBleu.getId()) {
          scoreDiceMBleu++;
        } else {
          scoreDiceMRouge++;
        }
      }
      if (dices.getValue() == DiceEntity.DRAW_CARD_FACE) {
        System.out.print(" [ DRAW CARD ] ");
      }
      if (dices.getValue() == DiceEntity.GIVE_DICE_FACE) {
        System.out.print(" [ GIVE DICE ] ");

        givedice(currentplayer);
      }
    }
    return new int[] {scoreDiceMBleu, scoreDiceMRouge};
  }
}
