package be.ipl.wazabi.test;
import java.util.Collection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.IUserUcc;
import be.ipl.wazabi.ucc.exception.UccException;

public class TestNG_GameUcc {


  private static IGameUcc gameUcc;
  private static IGameListUcc gameListUcc;
  private static IUserUcc userUcc;


  private static PlayerEntity player1;
  private static PlayerEntity player2;
  private static GameEntity game;

  @BeforeClass
  public static void setUpBeforeClass() throws UccException {
    try {
      Context jndi = new InitialContext();
      gameUcc = (IGameUcc) jndi.lookup("ejb:AAE_EAR/AAE_EJB/GameUcc!be.ipl.wazabi.ucc.IGameUcc");
      gameListUcc = (IGameListUcc) jndi
          .lookup("ejb:AAE_EAR/AAE_EJB/GameListUcc!be.ipl.wazabi.ucc.IGameListUcc");
      userUcc = (IUserUcc) jndi.lookup("ejb:AAE_EAR/AAE_EJB/UserUcc!be.ipl.wazabi.ucc.IUserUcc");
    } catch (NamingException e) {
      e.printStackTrace();
    }

    game = gameListUcc.createGame("GameTestUCCreator");
    player1 = userUcc.register("mikdom", "123456");
    player2 = userUcc.register("wiiki", "123456");
  }

  // The player is not in a game or does not exist.
  @Test(expectedExceptions = UccException.class)
  public void rollDicesPlayerNotInTheGame() throws UccException {
    gameUcc.rollDices(player1.getId());
  }

  // It is not the turn of this player to play.
  @Test(dependsOnMethods = "rollDicesPlayerNotInTheGame", expectedExceptions = UccException.class)
  public void rollDicesNotTheTurn() throws UccException {
    game = gameListUcc.joinGame(game.getId(), player1.getId());
    AssertJUnit.assertEquals(1, game.getPlayerCount());
    gameUcc.rollDices(player1.getId());
  }

  // The turn of this player to play.
  @Test(dependsOnMethods = "rollDicesNotTheTurn")
  public void rollDicesTheTurn() throws UccException {

    game = gameListUcc.joinGame(game.getId(), player2.getId());
    if (game.getCurrentPlayer().getPlayer().getId() == player1.getId()) {
      Collection<DiceEntity> dicesResult = gameUcc.rollDices(player1.getId());
      AssertJUnit.assertFalse(dicesResult == null);
    } else {
      Collection<DiceEntity> dicesResult = gameUcc.rollDices(player2.getId());
      AssertJUnit.assertFalse(dicesResult == null);
    }
  }

  // The player is not in a game or does not exist.
  @Test(expectedExceptions = UccException.class)
  public void giveDicesPlayerNotInGame() throws UccException {
    int[] destination = {1, 1};
    gameUcc.giveDices(player1.getId(), destination);
  }

  // It is not the turn of this player to play or he hasn't .
  @Test(dependsOnMethods = "rollDicesTheTurn", expectedExceptions = UccException.class)
  public void giveDicesNotTheTurn() throws UccException {
    int[] destination1 = {player2.getId()};
    int[] destination2 = {player1.getId()};
    int currentPlayerId =
        gameListUcc.getCurrentGame(player1.getId()).getCurrentPlayer().getPlayer().getId();
    if (currentPlayerId == player2.getId()) {
      gameUcc.giveDices(player1.getId(), destination1);
    }
    if (currentPlayerId == player1.getId()) {
      gameUcc.giveDices(player2.getId(), destination2);
    }

  }

  // Trying to give out more dices than allowed.
  @Test(dependsOnMethods = "rollDicesTheTurn", expectedExceptions = UccException.class)
  public void giveMoreDice() throws UccException {
    int idReceiver1 = player1.getId();
    int idReceiver2 = player2.getId();
    int[] destination1 = {idReceiver1, idReceiver1, idReceiver1, idReceiver1, idReceiver1};
    int[] destination2 = {idReceiver2, idReceiver2, idReceiver2, idReceiver2, idReceiver2};

    int currentPlayerId =
        gameListUcc.getCurrentGame(player1.getId()).getCurrentPlayer().getPlayer().getId();
    if (currentPlayerId == player1.getId()
        && game.getPlayerInstance(player1).getPlayableDices().size() < destination2.length) {
      gameUcc.giveDices(player1.getId(), destination2);
    }
    if (currentPlayerId == player2.getId()
        && game.getPlayerInstance(player2).getPlayableDices().size() < destination1.length) {
      gameUcc.giveDices(player2.getId(), destination1);
    }

  }

  // Suceed
  @Test(dependsOnMethods = "rollDicesTheTurn")
  public void giveDices() throws UccException {

    int[] destination1 = {player2.getId()};
    int[] destination2 = {player1.getId()};
    Collection<DiceEntity> dicesPlayer1 = gameUcc.getDiceCount(player1.getId());
    Collection<DiceEntity> dicesPlayer2 = gameUcc.getDiceCount(player2.getId());
    int dicesBefore1 = dicesPlayer1.size();
    int dicesBefore2 = dicesPlayer2.size();

    int currentPlayerId =
        gameListUcc.getGameById(game.getId()).getCurrentPlayer().getPlayer().getId();
    if (currentPlayerId == player1.getId() && dicesBefore1 >= destination2.length) {
      for (DiceEntity d : dicesPlayer1) {
        if (d.getValue() == DiceEntity.GIVE_DICE_FACE) {
          gameUcc.giveDices(player1.getId(), destination2);
          int dicesAfter1 = gameUcc.getDiceCount(player1.getId()).size();
          AssertJUnit.assertTrue(dicesBefore1 <= dicesAfter1);
          return;
        }
      }
    }
    
    if (currentPlayerId == player2.getId() && dicesBefore2 >= destination1.length) {
      for (DiceEntity d : dicesPlayer2) {
        if (d.getValue() == DiceEntity.GIVE_DICE_FACE) {
          gameUcc.giveDices(player2.getId(), destination2);
          int dicesAfter2 = gameUcc.getDiceCount(player2.getId()).size();
          AssertJUnit.assertTrue(dicesBefore2 <= dicesAfter2);
          return;
        }
      }
    }
  }

  // The player is not in a game or does not exist.
  @Test(expectedExceptions = UccException.class)
  public void getDiceCountPlayerNotInAGame() throws UccException {
    gameUcc.getDiceCount(player1.getId());
  }

  // Suceed : Player in the game
  @Test(dependsOnMethods = "rollDicesTheTurn")
  public void getDiceCountPlayerInAGame() throws UccException {
    Collection<DiceEntity> handDicePlayer1 = gameUcc.getDiceCount(player1.getId());
    Collection<DiceEntity> handDicePlayer2 = gameUcc.getDiceCount(player2.getId());
    AssertJUnit.assertTrue(handDicePlayer1.size() > 0 && handDicePlayer2.size() > 0);

  }

  // The player is not in a game or does not exist.
  @Test(expectedExceptions = UccException.class)
  public void getCardsPlayerNotInAGame() throws UccException {
    gameUcc.getCards(player1.getId());
  }

  // Suceed : Player in the game
  @Test(dependsOnMethods = "rollDicesTheTurn")
  public void getCardsPlayerInAGame() throws UccException {
    Collection<CardEntity> handCardPlayer1 = gameUcc.getCards(player1.getId());
    Collection<CardEntity> handCardPlayer2 = gameUcc.getCards(player2.getId());
    AssertJUnit.assertTrue(handCardPlayer1.size() > 0 && handCardPlayer2.size() > 0);

  }

  // The player is not in a game or does not exist.
  @Test(expectedExceptions = UccException.class)
  public void quitGamePlayerNotInGame() throws UccException {
    gameUcc.quitGame(player1.getId());
  }

  // Suceed : Player in the game
  @Test(dependsOnMethods = "giveMoreDice")
  public void quitGame() throws UccException {

    game = gameListUcc.getCurrentGame(player2.getId());
    AssertJUnit.assertTrue(game.getId() == gameListUcc.getCurrentGame(player1.getId()).getId());
    gameUcc.quitGame(player1.getId());
    game = gameListUcc.getCurrentGame(player2.getId());
    AssertJUnit.assertTrue(game == null);

  }


  // The player is not in a game or does not exist.
  @Test(expectedExceptions = UccException.class)
  void playCardPlayerNotInTheGame() throws UccException {
    gameUcc.playCard(player1.getId(), -1, "test");
  }

  // The player is not in a game or does not exist.
  @Test(dependsOnMethods = "getCardsPlayerInAGame", expectedExceptions = UccException.class)
  void playCardNotTheTurn() throws UccException {
    game = gameListUcc.getCurrentGame(player1.getId());
    if (game.getCurrentPlayer().getPlayer().getId() == player1.getId()) {
      gameUcc.playCard(player2.getId(), -1, "test");
    } else {
      gameUcc.playCard(player1.getId(), -1, "test");
    }

  }

  // The player doesn't have enough {@link DiceEntity#WAZABI_FACE} dices to play this card.
  @Test(dependsOnMethods = "rollDicesTheTurn", expectedExceptions = UccException.class)
  void playCardNotDiceToPlayThisCard() throws UccException {
    game = gameListUcc.getCurrentGame(player1.getId());
    int pointCard = 0;
    for (DiceEntity dice : game.getCurrentPlayer().getPlayableDices()) {
      if (dice.getValue() == DiceEntity.WAZABI_FACE) {
        pointCard++;
      }
    }
    for (CardEntity card : game.getCurrentPlayer().getPlayableCards()) {
      if (card.getEffect().getUseCost() > pointCard) {
        gameUcc.playCard(game.getCurrentPlayer().getPlayer().getId(), card.getId(), "test");
      }
    }
  }

  // The card is not owned by this player.
  @Test(dependsOnMethods = "getCardsPlayerInAGame", expectedExceptions = UccException.class)
  void playCardNotOwnedByThisPlayer() throws UccException {
    game = gameListUcc.getCurrentGame(player1.getId());
    gameUcc.playCard(game.getCurrentPlayer().getPlayer().getId(), 1111, "test");
  }

  // Invalid card parameter
  @Test(dependsOnMethods = "getCardsPlayerInAGame", expectedExceptions = UccException.class)
  void playCardInvalidCardParameter() throws UccException {
    game = gameListUcc.getCurrentGame(player1.getId());
    for (CardEntity card : game.getCurrentPlayer().getPlayableCards()) {
      gameUcc.playCard(game.getCurrentPlayer().getPlayer().getId(), card.getId(), "");
    }

  }

  // Succeed
  @Test(dependsOnMethods = "getCardsPlayerInAGame", expectedExceptions = UccException.class)
  void playCard() throws UccException {
    game = gameListUcc.getCurrentGame(player1.getId());
    for (CardEntity card : game.getCurrentPlayer().getPlayableCards()) {
      gameUcc.playCard(game.getCurrentPlayer().getPlayer().getId(), card.getId(), "test");
    }

  }

}

